// ignore_for_file: camel_case_types
import 'package:deeply/howfeel_01.dart';
import 'package:flutter/material.dart';
import 'package:deeply/calls.dart';
import 'package:flutter/services.dart';
import 'package:deeply/diary.dart';
import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;
import 'package:audioplayers/audioplayers.dart';


final Bad_feelings = [
  'Bored',
  'Busy',
  'Stressed',
  'Tired',
];
final subfeelings1 = ['Indifferent', 'Pressured', 'Overwhelmed', 'Sleepy'];
final subfeelings2 = ['Apathetic', 'Rushed', 'Out of control', 'Unfocused'];

class Item {
  int id;
  String expandedValue1;
  String expandedValue2;
  String headerValue;

  Item({
    required this.id,
    required this.expandedValue1,
    required this.expandedValue2,
    required this.headerValue,
  });
  Item.fromMap(Map<String, dynamic> logs)
      : id = logs['id'],
        expandedValue1 = logs['expandedValue1'],
        expandedValue2 = logs['expandedValue2'],
        headerValue = logs['headerValue'];

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'expandedValue1': expandedValue1,
      'expandedValue2': expandedValue2,
      'headerValue': headerValue,
      'month': DateTime.now().month,
      'day': DateTime.now().day
    };
  }
}

class SQLiteService {
  Future<Database> initDB() async {
    return openDatabase(
      p.join(await getDatabasesPath(), 'd.db'), // opens and returns a handle or creates the db
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE logs(logID INTEGER PRIMARY KEY AUTOINCREMENT,id INTEGER, month INTEGER, day INTEGER, expandedValue1 STRING, expandedValue2 STRING, headerValue STRING)'
        ); // IF NOT EXISTS
      },
      version: 1,
    );
  }

  Future<List<Item>> getLogs() async{
  final db = await initDB();

  final List<Map<String, Object?>> queryResult = await db.query('logs');
  return queryResult.map((e) => Item.fromMap(e)).toList();
}

  Future<int> addLog(Item item) async {
  final db = await initDB();

  return db.insert('logs', item.toMap(),
    conflictAlgorithm: ConflictAlgorithm.replace);
}

}

class MyClass {
  static Future<void> printDatabasePath() async {
    String path = await getDatabasesPath();
    print('Path to database: $path');
  }
}

List<Item> generateItems(int numberOfItems) {
  return List<Item>.generate(numberOfItems, (int index) {
    return Item(
      id: index,
      headerValue: Bad_feelings[index],
      expandedValue1: subfeelings1[index],
      expandedValue2: subfeelings2[index],
    );
  });
}

class BMyStatefulWidget extends StatefulWidget {
  const BMyStatefulWidget({super.key});

  @override
  State<BMyStatefulWidget> createState() => _BMyStatefulWidgetState();
}

class _BMyStatefulWidgetState extends State<BMyStatefulWidget> {
  final List<Item> _data = generateItems(4);

  late SQLiteService sqLiteService;
  List<Item> _items = <Item>[];     

 @override
  void initState() {
    super.initState(); //NEEDS A KEY IN THE CONSTRUCTOR

    /// Αρχικοποίηση στιγμιοτύπου της κλάσης [SQLiteService] για τη δημιουργία
    /// και σύνδεση με την SQLite βάση δεδομένων της εφαρμογής
    sqLiteService = SQLiteService();

    /// Κλήση της μεθόδου initDB() και φόρτωση της λίστας με τα tasks που πιθανώς
    /// να υπάρχουν στη βάση
    sqLiteService.initDB().whenComplete(() async {
      final items = await sqLiteService.getLogs();

      setState(() {
        _items = items;
      });
    });}

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _buildPanel(),
      ),
    );
  }

  Widget _buildPanel() {
    return ExpansionPanelList.radio(
      initialOpenPanelValue: 0,
      children: _data.map<ExpansionPanelRadio>((Item item) {
        return ExpansionPanelRadio(
            value: item.id,
            headerBuilder: (BuildContext context, bool isExpanded) {
              return ListTile(
                title: Text(item.headerValue),
              );
            },
            body: Column(
              children: <Widget>[
                ListTile(
                    title: Text(item.expandedValue1),
                    trailing: const Icon(Icons.save_alt),
                    onTap: () async {
                      var log1 = Item(
                        id: item.id,
                        expandedValue1: item.expandedValue1,
                        expandedValue2: item.expandedValue1,
                        headerValue: item.headerValue,
                      );                    
                      await sqLiteService.addLog(log1);
                    }),
                ListTile(
                    title: Text(item.expandedValue2),
                    trailing: const Icon(Icons.save_alt),
                    onTap: () async {
                      var log1 = Item(
                        id: item.id,
                        expandedValue1: item.expandedValue2,
                        expandedValue2: item.expandedValue2,
                        headerValue: item.headerValue,
                      );
                      await sqLiteService.addLog(log1);
                      /*setState(() {
                    _data
                        .removeWhere((Item currentItem) => item == currentItem);
                  });*/
                    })
              ],
            ));
      }).toList(),
    );
  }
}

class Bad_Layer2_Widget extends StatefulWidget {
  const Bad_Layer2_Widget({Key? key}) : super(key: key); //optional params

  @override
  State<Bad_Layer2_Widget> createState() => _Bad_Layer2_WidgetState();
}

class _Bad_Layer2_WidgetState extends State<Bad_Layer2_Widget> {
   AudioPlayer _audioPlayer = AudioPlayer();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.amber.shade50,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70),
        child: AppBar(
          //leading: Icon(Icons.home),
          title: const Text('Bad'),
          centerTitle: true,
          leading: IconButton(
              icon: Image.asset(
                'assets/SmallFAB.png',
              ),
              onPressed: () {
                if (scaffoldKey.currentState!.isDrawerOpen) {
                  scaffoldKey.currentState!.closeDrawer();
                  //close drawer, if drawer is open
                } else {
                  scaffoldKey.currentState!.openDrawer();
                  //open drawer, if drawer is closed
                }
              }),
          backgroundColor: Colors.deepPurple, //color
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.amber.shade50,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.deepPurple,
              ),
              child: Text('Menu',
                  style: TextStyle(fontSize: 25, color: Colors.white)),
            ),
            ListTile(
              leading: const Icon(
                Icons.home, // change icon
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HowFeel()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.history, //change icon
              ),
              title: const Text('Diary'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Diary()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.close, //change icon
              ),
              title: const Text('Exit'),
              onTap: () => SystemNavigator.pop(),
            ),
          ],
        ),
      ),
      body: Column(children: [
        Expanded(
          child: BMyStatefulWidget(),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                //height: 70,
                padding: const EdgeInsets.symmetric(vertical: 35.0),
                child: SizedBox(
                    height: 60,
                    width: 188,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(90))),
                        onPressed: () async {
                          // To call the method:
                          await MyClass.printDatabasePath();
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const CallsWidget()),
                          );
                        },
                        child: const Text(
                          'Go Deep',
                          style: TextStyle(fontSize: 30),
                        )))))
      ]),
    );
  }
}
