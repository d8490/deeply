import 'package:audioplayers/audioplayers.dart';
import 'package:deeply/BadLayer_03.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:deeply/howfeel_01.dart';
import 'package:deeply/diary.dart';
import 'package:deeply/audio.dart';
import 'package:vibration/vibration.dart';
import 'dart:io';

class ground_music extends StatefulWidget {
  const ground_music({Key? key}): super(key:key); //optional params

  @override
  _ground_musicState createState() => _ground_musicState();
}

class  _ground_musicState extends State<ground_music> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){
    return GestureDetector(
      onHorizontalDragUpdate: (details) {
        if (details.delta.dx < 0) {
          Vibration.vibrate(duration: 6000, amplitude: 128, intensities: [1, 3]);
          // Navigate to MusicPlayer screen if left swipe
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MusicPlayer()),
          );
        }
        print('Horizontal drag update: ${details.delta.dx}');
      },
      child: Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.amber.shade50,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70),
        child: AppBar(
          //leading: Icon(Icons.home),
          title: const Text('Catch up'),
          centerTitle: true,
          leading: IconButton(
              icon: Image.asset(
                'assets/SmallFAB.png',
              ),
              onPressed: () {
                if (scaffoldKey.currentState!.isDrawerOpen) {
                  scaffoldKey.currentState!.closeDrawer();
                  //close drawer, if drawer is open
                } else {
                  scaffoldKey.currentState!.openDrawer();
                  //open drawer, if drawer is closed
                }
              }),
          backgroundColor: Colors.deepPurple, //color
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.amber.shade50,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.deepPurple,
              ),
              child: Text('Menu',
                  style: TextStyle(fontSize: 25, color: Colors.white)),
            ),
            ListTile(
              leading: const Icon(
                Icons.home, // change icon
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HowFeel()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.history, //change icon
              ),
              title: const Text('Diary'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                MaterialPageRoute(builder: (context) => const Diary()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.close, //change icon
              ),
              title: const Text('Exit'),
              onTap: () => SystemNavigator.pop(),
            ),
          ],
        ),
      ),

      body: Column(
        children : <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child:Padding(
              padding: EdgeInsets.symmetric(vertical: 170.0),
              child: Column(
                children: [
                  new Icon(Icons.headphones, size: 200.0, color: Color.fromARGB(255, 76, 5, 127),),
                  const Text("Connect your headphones"),
                ],),),),
                    Expanded(
                      child: Icon(Icons.swipe_down_outlined, size: 50.0), 
                    ),
                ],),),);
  }  
}


