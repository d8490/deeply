// ignore_for_file: prefer_typing_uninitialized_variables, non_constant_identifier_names
import 'package:deeply/howfeel_01.dart';
import 'package:deeply/BadLayer_03.dart';
import 'package:deeply/music.dart';
//import 'package:deeply/ground_music.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:deeply/diary.dart';

void main() {
  runApp(const Layer1());
}

final feelings = [
  'Bad',
  'Happy',
  'Angry',
  'Fearful',
  'Sad',
  'Disgusted',
  'Surprised',
  'Love'
];

class Layer1 extends StatefulWidget {
  const Layer1({Key? key}) : super(key: key); //optional params

  @override
  State<Layer1> createState() => _Layer1State();
}

class _Layer1State extends State<Layer1> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  int _selectedIndex = -1;
  late Widget next_route = _selectedIndex == -1 ? const Layer1() : const ground_music();//const SizedBox();
  //late Widget next_route; // it will be initialized later
  @override
  Widget build(BuildContext context) {
    //next_route = const Layer1(); // initialize 
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.amber.shade50,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70),
        child: AppBar(
          //leading: Icon(Icons.home),
          title: const Text('Layer 1'),
          centerTitle: true,
          leading: IconButton(
              icon: Image.asset(
                'assets/SmallFAB.png',
              ),
              onPressed: () {
                if (scaffoldKey.currentState!.isDrawerOpen) {
                  scaffoldKey.currentState!.closeDrawer();
                  //close drawer, if drawer is open
                } else {
                  scaffoldKey.currentState!.openDrawer();
                  //open drawer, if drawer is closed
                }
              }),
          backgroundColor: Colors.deepPurple, //color
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.amber.shade50,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.deepPurple,
              ),
              child: Text('Menu',
                  style: TextStyle(fontSize: 25, color: Colors.white)),
            ),
            ListTile(
              leading: const Icon(
                Icons.home, // change icon
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HowFeel()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.history, //change icon
              ),
              title: const Text('Diary'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Diary()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.close, //change icon
              ),
              title: const Text('Exit'),
              onTap: () => SystemNavigator.pop(),
            ),
          ],
        ),
      ),
      body: Column(children: [
        Expanded(
            child: ListView.builder(
                itemCount: feelings.length,
                itemBuilder: (context, index) {
                  return ListTile(
                      dense: true,
                      visualDensity: const VisualDensity(vertical: 4),
                      selectedTileColor: Colors.deepPurple.shade50,
                      title: Text(feelings[index],
                          style: const TextStyle(fontSize: 25)),
                      selected: index == _selectedIndex,
                      leading: index == _selectedIndex
                          ? const Icon(
                              Icons.lens,
                            )
                          : const Icon(
                              Icons.radio_button_off,
                            ),
                      onTap: () {
                        setState(() {
                          _selectedIndex = index;
                          switch (_selectedIndex) {
                            case -1:
                              {
                                next_route = const ground_music();
                              }
                              break;
                            case 0:
                              {
                                next_route = const ground_music();
                              }
                              break;

                            case 1:
                              {
                                next_route = const ground_music();
                              }
                              break;

                            case 2:
                              {
                                next_route = const Layer1();
                              }
                              break;

                            case 3:
                              {
                                next_route = const Layer1();
                              }
                              break;

                            case 4:
                              {
                                next_route = const Layer1();
                              }
                              break;

                            case 5:
                              {
                                next_route = const Layer1();
                              }
                              break;

                            case 6:
                              {
                                next_route = const Layer1();
                              }
                              break;

                            case 7:
                              {
                                next_route = const Layer1();
                              }
                              break;

                            default:
                              {
                                next_route = const Layer1();
                              }
                              break;
                          }
                        });
                      });
                })),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                //height: 70,
                padding: const EdgeInsets.symmetric(vertical: 35.0),
                child: SizedBox(
                    height: 60,
                    width: 188,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(90))),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => next_route),
                          );
                        },
                        child: const Text(
                          'Go Deep',
                          style: TextStyle(fontSize: 30),
                        )))))
      ]),
    );
  }
}
