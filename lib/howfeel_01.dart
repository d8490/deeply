// main.dart
import 'package:deeply/layer1_02.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:deeply/main.dart';
import 'package:deeply/diary.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'HowRUFeeling',
      theme: ThemeData(primarySwatch: Colors.deepPurple),
      home: const HowFeel(),
    );
  }
}

class HowFeel extends StatefulWidget {
  const HowFeel({Key? key}) : super(key: key);

  @override
  State<HowFeel> createState() => _HowFeelState();
}

class _HowFeelState extends State<HowFeel> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.amber.shade50,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70),
        // ignore: sort_child_properties_last
        child: AppBar(
            centerTitle: true,
            leading: IconButton(
                icon: Image.asset(
                  'assets/SmallFAB.png',
                ),
                onPressed: () {
                  if (scaffoldKey.currentState!.isDrawerOpen) {
                    scaffoldKey.currentState!.closeDrawer();
                    //close drawer, if drawer is open
                  } else {
                    scaffoldKey.currentState!.openDrawer();
                    //open drawer, if drawer is closed
                  }
                })),
      ),
      drawer: Drawer(
        backgroundColor: Colors.amber.shade50,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.deepPurple,
              ),
              child: Text('Menu',
                  style: TextStyle(fontSize: 25, color: Colors.white)),
            ),
            ListTile(
              leading: const Icon(
                Icons.home, // change icon
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HowFeel()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.history, //change icon
              ),
              title: const Text('Diary'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Diary()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.close, //change icon
              ),
              title: const Text('Exit'),
              onTap: () => SystemNavigator.pop(),
            ),
          ],
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // display the entered numbers
          Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'How are you\nfeeling\ntoday?',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.deepPurple.shade900, fontSize: 50),
                  ),
                  Flexible(
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 35.0),
                              child: SizedBox(
                                  height: 80,
                                  width: 250,
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(90))),
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Layer1()));
                                      },
                                      child: const Text(
                                        'Go Deep',
                                        style: TextStyle(fontSize: 40),
                                      ))))))
                ],
              )),
        ],
      ),
    );
  }
}
