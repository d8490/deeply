import 'dart:io';
import 'package:deeply/BadLayer_03.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';
import 'dart:typed_data';
import 'package:audioplayers/audioplayers.dart';
import 'package:path_provider/path_provider.dart';
import 'package:deeply/howfeel_01.dart';
import 'package:deeply/diary.dart';


class MusicPlayer extends StatefulWidget {
  @override
  _MusicPlayerState createState() => _MusicPlayerState();
}

class _MusicPlayerState extends State<MusicPlayer> {
  AudioPlayer _audioPlayer = AudioPlayer();
  final scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragUpdate: (details) {
        if (details.delta.dx < 0) {
          // Navigate to MusicPlayer screen if left swipe
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Bad_Layer2_Widget()),
          );
        }
        print('Horizontal drag update: ${details.delta.dx}');
      },
      child: Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.amber.shade50,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70),
        child: AppBar(
          //leading: Icon(Icons.home),
          title: const Text('Catch up'),
          centerTitle: true,
          leading: IconButton(
              icon: Image.asset(
                'assets/SmallFAB.png',
              ),
              onPressed: () {
                if (scaffoldKey.currentState!.isDrawerOpen) {
                  scaffoldKey.currentState!.closeDrawer();
                  //close drawer, if drawer is open
                } else {
                  scaffoldKey.currentState!.openDrawer();
                  //open drawer, if drawer is closed
                }
              }),
          backgroundColor: Colors.deepPurple, //color
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.amber.shade50,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.deepPurple,
              ),
              child: Text('Menu',
                  style: TextStyle(fontSize: 25, color: Colors.white)),
            ),
            ListTile(
              leading: const Icon(
                Icons.home, // change icon
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HowFeel()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.history, //change icon
              ),
              title: const Text('Diary'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                MaterialPageRoute(builder: (context) => const Diary()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.close, //change icon
              ),
              title: const Text('Exit'),
              onTap: () => SystemNavigator.pop(),
            ),
          ],
        ),
      ),

        body: 
        
        Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Padding(
          padding: const EdgeInsets.only(top: 160.0),
          child: 
      ElevatedButton(
        onPressed: () async {
          String audioUrl = 'assets/audios/song.mp3';
          final ByteData data = await rootBundle.load(audioUrl);
          // final Directory tempDir = Directory.systemTemp;
          final Directory tempDir = await getTemporaryDirectory();
          final File file = File('${tempDir.path}/audio.mp3');
          await file.writeAsBytes(data.buffer.asUint8List(), flush: true);
          _audioPlayer.play(file.path, isLocal: true);
          //{Navigator.push(context,MaterialPageRoute(builder: (context) => Bad_Layer2_Widget()),);}
        },
        child: Text('Play Audio'),
      ),),
      Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0),
          child:
      ElevatedButton(
        onPressed: () async {
          // stop audio
          await _audioPlayer.stop();
          //{Navigator.push(context,MaterialPageRoute(builder: (context) => Bad_Layer2_Widget()),);}
        },
        child: Text("Stop Audio"),
      ),),
       Expanded(child: Icon(Icons.swipe_down_outlined, size: 50.0),),
    ]))));
  }
}
