import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:deeply/howfeel_01.dart';
import 'package:deeply/diary.dart';
import 'package:permission_handler/permission_handler.dart';

Future<void> requestPhoneCallPermission() async {
  final PermissionStatus permissionStatus = await Permission.phone.request();
  if (permissionStatus != PermissionStatus.granted) {
    throw 'Phone call permission not granted';
  }
}

_makingPhoneCall() async {
  var url = Uri.parse("tel:");
  if (await canLaunchUrl(url)) {
    await launchUrl(url);
  } else {
    throw 'Could not launch phone call app';
  }
}

_makingPhoneCall2() async {
  var url = Uri.parse("tel:10306");
  if (await canLaunchUrl(url)) {
    await launchUrl(url);
  } else {
    throw 'Could not launch phone call app';
  }
}

class CallsWidget extends StatefulWidget {
  const CallsWidget({Key? key}) : super(key: key); //optional params

  @override
  _CallsWidgetState createState() => _CallsWidgetState();
}

class _CallsWidgetState extends State<CallsWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.amber.shade50,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70),
        child: AppBar(
          //leading: Icon(Icons.home),
          title: const Text('Catch up'),
          centerTitle: true,
          leading: IconButton(
              icon: Image.asset(
                'assets/SmallFAB.png',
              ),
              onPressed: () {
                if (scaffoldKey.currentState!.isDrawerOpen) {
                  scaffoldKey.currentState!.closeDrawer();
                  //close drawer, if drawer is open
                } else {
                  scaffoldKey.currentState!.openDrawer();
                  //open drawer, if drawer is closed
                }
              }),
          backgroundColor: Colors.deepPurple, //color
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.amber.shade50,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.deepPurple,
              ),
              child: Text('Menu',
                  style: TextStyle(fontSize: 25, color: Colors.white)),
            ),
            ListTile(
              leading: const Icon(
                Icons.home, // change icon
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HowFeel()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.history, //change icon
              ),
              title: const Text('Diary'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                MaterialPageRoute(builder: (context) => const Diary()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.close, //change icon
              ),
              title: const Text('Exit'),
              onTap: () => SystemNavigator.pop(),
            ),
          ],
        ),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage("assets/happy_image.png"),
              fit: BoxFit.cover,),
          
            )
          ),
          Center( 
            child: Column(children: <Widget>[
      Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 90.0),
          child: Column(
            children: [
              SizedBox(
                  height: 150.0,
                  width: 150.0,
                  child: IconButton(
                    padding: const EdgeInsets.all(0.0),
                    color: Color.fromARGB(255, 76, 5, 127),
                    icon: const Icon(Icons.people_outlined, size: 150.0),
                    onPressed: () async {
                        var status = await Permission.phone.request();
                        if (status == PermissionStatus.granted) {
                          _makingPhoneCall();
                        }}
                  )),
              Text("Call a friend"),
            ],
          ),
        ),
      ),
      Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 30.0),
            child: Column(
              children: [
                new SizedBox(
                    height: 150.0,
                    width: 150.0,
                    child: new IconButton(
                      padding: new EdgeInsets.all(0.0),
                      color: Color.fromARGB(255, 76, 5, 127),
                      icon: new Icon(Icons.psychology_outlined, size: 150.0),
                        onPressed: () async {
                        var status = await Permission.phone.request();
                        if (status == PermissionStatus.granted) {
                          _makingPhoneCall2();
                        }}
                    )),
                Text(
                  "Call an expert",
                )
              ],
            ),
          )),
            ],),),],),);}      
}
