import 'package:deeply/howfeel_01.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:intl/intl.dart';
import 'package:deeply/logs.dart';

void main() {
  runApp(const Diary());
}

class Diary extends StatefulWidget {
  const Diary({Key? key}) : super(key: key); //optional params

  @override
  State<Diary> createState() => _DiaryState();
}

class _DiaryState extends State<Diary> {
  //TextEditingController dateInput = TextEditingController();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.amber.shade50,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(70),
          child: AppBar(
            //leading: Icon(Icons.home),
            title: const Text('Diary'),
            centerTitle: true,
            leading: IconButton(
                icon: Image.asset(
                  'assets/SmallFAB.png',
                ),
                onPressed: () {
                  if (scaffoldKey.currentState!.isDrawerOpen) {
                    scaffoldKey.currentState!.closeDrawer();
                    //close drawer, if drawer is open
                  } else {
                    scaffoldKey.currentState!.openDrawer();
                    //open drawer, if drawer is closed
                  }
                }),
            backgroundColor: Colors.deepPurple, //color
          ),
        ),
        drawer: Drawer(
          backgroundColor: Colors.amber.shade50,
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.deepPurple,
                ),
                child: Text('Menu',
                    style: TextStyle(fontSize: 25, color: Colors.white)),
              ),
              ListTile(
                leading: const Icon(
                  Icons.home, // change icon
                ),
                title: const Text('Home'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => const HowFeel()));
                },
              ),
              ListTile(
                leading: const Icon(
                  Icons.history, //change icon
                ),
                title: const Text('Diary'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => const Diary()));
                },
              ),
              ListTile(
                leading: const Icon(
                  Icons.close, //change icon
                ),
                title: const Text('Exit'),
                onTap: () => SystemNavigator.pop(),
              ),
            ],
          ),
        ),
        body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text("${selectedDate.toLocal()}".split(' ')[0]),
            const SizedBox(height: 20.0,),
            //BMyStatefulWidget.getLogs()
            ElevatedButton(
              onPressed: () => _selectDate(context),
              child: const Text('Select date'),
            ),
            const SizedBox(height: 20.0,),
            //BMyStatefulWidget.getLogs()
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  Logs()), //
                        );
              },
              child: const Text('See logs'),
            ),
          ],
        ),
      ),
        
        /*Container(
            padding: const EdgeInsets.all(15),
            height: MediaQuery.of(context).size.width / 3,
            child: Center(
                child: TextField(
              controller: dateInput,
              //editing controller of this TextField
              decoration: const InputDecoration(
                  icon: Icon(Icons.calendar_today), //icon of text field
                  labelText: "Enter Date" //label text of field
                  ),
              readOnly: true,
              //set it true, so that user will not able to edit text
              onTap: () async {
                DateTime? pickedDate = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(1950),
                    //DateTime.now() - not to allow to choose before today.
                    lastDate: DateTime(2100));

                if (pickedDate != null) {
                  print(
                      pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                  String formattedDate =
                      DateFormat('yyyy-MM-dd').format(pickedDate);
                  print(
                      formattedDate); //formatted date output using intl package =>  2021-03-16
                  setState(() {
                    dateInput.text =
                        formattedDate; //set output date to TextField value.
                  });
                } else {}
              },
            )))*/);
  }
}
