//import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:deeply/howfeel_01.dart';

// import our custom number keyboard
import './num_pad.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const Deeply());
}

class Deeply extends StatelessWidget {
  const Deeply({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Deeply',
        theme: ThemeData(
          primarySwatch: Colors.deepPurple,
        ),
         home: const MyApp(),);
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Deeply',
      theme: ThemeData(primarySwatch: Colors.deepPurple),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // text controller
  final TextEditingController _myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber.shade50,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Insert PIN'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          // display the entered numbers
          Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/lock.png',
                  ),
                  SizedBox(
                    height: 70,
                    child: Center(
                        child: TextField(
                      obscureText: true,
                      controller: _myController,
                      textAlign: TextAlign.center,
                      showCursor: false,
                      style: const TextStyle(fontSize: 40),
                      // Disable the default soft keybaord
                      keyboardType: TextInputType.none,
                    )),
                  ),
                ],
              )),
          // implement the custom NumPad
          NumPad(
            buttonSize: 75,
            buttonColor: Colors.deepPurple.shade900,
            iconColor: Colors.deepPurple.shade300,
            controller: _myController,
            delete: () {
              _myController.text = _myController.text
                  .substring(0, _myController.text.length - 1);
            },
            // do something with the input numbers
            onSubmit: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const HowFeel()));
            },
          ),
        ],
      ),
    );
  }
}
