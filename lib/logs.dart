import 'package:flutter/material.dart';
import 'package:deeply/calls.dart';
import 'package:path/path.dart' as p;
import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:deeply/BadLayer_03.dart';
import 'package:deeply/diary.dart';
import 'package:deeply/howfeel_01.dart';
import 'package:flutter/services.dart';

class SQLiteService{
Future<Database> initDB() async{
    return openDatabase(
      p.join(await getDatabasesPath(), 'd.db'),  //I have to delete the existing database file from the C:->Users->georg (or create a new one after hot reload) because otherwise I get an error 
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE IF NOT EXISTS logs(logID INTEGER PRIMARY KEY AUTOINCREMENT,id INTEGER, month INTEGER, day INTEGER, expandedValue1 STRING, expandedValue2 STRING, headerValue STRING)' // IF NOT EXISTS 
        );
      },
      version: 1,
      );
}

Future<List<Item>> getLogs() async{
  final db = await initDB();

  final List<Map<String, Object?>> queryResult = await db.query('logs');
  return queryResult.map((e) => Item.fromMap(e)).toList();
}
}

class Showlogs extends StatefulWidget {
  const Showlogs ({Key? key}): super(key:key); //({Key? key, required this.date}) : super(key: key);

  //final DateTime date;
  
  @override
  State<Showlogs> createState() => _ShowlogsState();
}

class _ShowlogsState extends State<Showlogs> {

late SQLiteService sqLiteService;

List<Item> _items = <Item>[];

 @override
  void initState() {
    super.initState();

    /// Αρχικοποίηση στιγμιοτύπου της κλάσης [SQLiteService] για τη δημιουργία
    /// και σύνδεση με την SQLite βάση δεδομένων της εφαρμογής
    sqLiteService = SQLiteService();

    /// Κλήση της μεθόδου initDB() και φόρτωση της λίστας με τα tasks που πιθανώς
    /// να υπάρχουν στη βάση
    sqLiteService.initDB().whenComplete(() async {
      final items = await sqLiteService.getLogs();

      int i = 0;
      //DateTime selected_date = Showlogs.date;
      setState(() {
        _items = items;
        /*if (items[i].month == date.month) { //${person.name}
          _items = items;
        }*/
        
      });
    });}

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _buildPanel(),
      ),
    );
  }

  Widget _buildPanel() {
    List<Item> data_;
    data_ = _items;
    return ExpansionPanelList.radio(
      initialOpenPanelValue: 0,
      children: data_.map<ExpansionPanelRadio>((Item item) {
        return ExpansionPanelRadio(
            value: item.id,
            headerBuilder: (BuildContext context, bool isExpanded) {
              return ListTile(
                subtitle: Text('Bad'),
                title: Text(item.headerValue),
              );
            },
            body: Column(
              children: <Widget> [
                ListTile(
                  title: Text(item.expandedValue1),
                ),],));
          }).toList(),
    );
  }
}

class Logs extends StatefulWidget {
  const Logs ({Key? key}): super(key:key);//({Key? key, required this.LogsDate}) : super(key: key);// //optional params

  //final DateTime LogsDate;

  @override
  _LogsState createState() => _LogsState();
}

class  _LogsState extends State<Logs> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context){
    //widget.LogsDate;
     return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.amber.shade50,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70),
        child: AppBar(
          //leading: Icon(Icons.home),
          title: const Text('Logs'),
          centerTitle: true,
          leading: IconButton(
              icon: Image.asset(
                'assets/SmallFAB.png',
              ),
              onPressed: () {
                if (scaffoldKey.currentState!.isDrawerOpen) {
                  scaffoldKey.currentState!.closeDrawer();
                  //close drawer, if drawer is open
                } else {
                  scaffoldKey.currentState!.openDrawer();
                  //open drawer, if drawer is closed
                }
              }),
          backgroundColor: Colors.deepPurple, //color
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.amber.shade50,
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.deepPurple,
              ),
              child: Text('Menu',
                  style: TextStyle(fontSize: 25, color: Colors.white)),
            ),
            ListTile(
              leading: const Icon(
                Icons.home, // change icon
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HowFeel()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.history, //change icon
              ),
              title: const Text('Diary'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Diary()));
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.close, //change icon
              ),
              title: const Text('Exit'),
              onTap: () => SystemNavigator.pop(),
            ),
          ],
        ),
      ),
      body: Column(children: [
          Expanded(
              child: Showlogs(),),//Showlogs(date: widget.LogsDate),),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 35.0),
                child: 
                SizedBox(
                    height: 60,
                    width: 188,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(90))),
                        onPressed: () async {
                          // To call the method:
                          await MyClass.printDatabasePath();
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const CallsWidget()),
                          );
                        },
                        child: const Text('Wanna Talk?',
                          style: TextStyle(fontSize: 20),
                        )))
                
                /*ElevatedButton(
                    onPressed: () {
                      {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  CallsWidget()),
                        );
                      }
                    },
                    child: const Text('Wanna Talk?')),*/
              )),
        ]
      
          ),); 
  }
}



